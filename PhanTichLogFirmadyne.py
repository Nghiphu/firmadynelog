import codecs
import string
import re
import pandas as pd
class ScLog:
    def __init__(self, pid, sc):
        self.pid = pid
        self.process = ''
        self.sc = sc
    def hienthi(self):
        print (str(self.process) + " : " + str(self.pid) + " : " + str(len(self.sc)))
        #print (self.sc)
    def luu(self, path):
        fo = open(path, 'w')
        fo.write(self.pid)
        for s in self.sc:
            fo.write(str(s) + "(")
        fo.close()

class FirmadyneLog:
    def __init__(self, path):
        self.path = path
        dem = 0
        fr = []
        with codecs.open(path, 'r', encoding='latin-1') as f:
            for line in f:
                line = str(line).strip()
                tg, sc, pid, pro = self.tachXau(line)
                if (tg != ""):
                    fr.append([tg, sc, int(pid), pro])

        self.dt = pd.DataFrame(fr)
        self.dt.columns = ["thoigian", "syscall", "pid", "process"]

    def tachXau(self, cmd=""):
        #cmd = "[ 1791.696000] fm: read[PID: 367 (snmpd)]: fd:13, size:4096"
        if cmd == "":
            #cmd = "[1795.428000] fm: time[PID: 315 (lighttpd)]:"
            cmd = "[   16.704000] fm: open[PID: 316 (rcS)]: file:/etc/init.d/S090translators.sh"

        lech = -1;
        cmd = cmd.strip()
        cmd = re.sub(' +', ' ', cmd)
        cmds = cmd.split(" ")

        tg = scName = pid = proName = ""

        # neu khong du so phan tu toi thieu
        if (len(cmds) <5):
            return "","","",""

        # lay thoi gian
        if ((cmds[0] == "[") and (cmds[2] == "fm:")):
            tg = cmds[1]
            tg = tg[0:len(tg)-1]
            lech = 1
        elif ((cmds[1] == "fm:")):
            tg = cmds[0]
            if (tg[0] == "[") and (tg[len(tg)-1] == "]"):
                tg = tg[1:len(tg) - 1]
                lech = 0

        if lech > -1:
            # lay ten syscall
            scName = cmds[lech + 2]
            if (scName[-5:] == "[PID:"):
                scName = scName[:len(scName)-5]
            else:
                scName = ""

            # lay pid
            pid = cmds[lech + 3]
            if (not pid.isnumeric()):
                pid = ""

            # lay ten process
            if (lech + 4 < len(cmds)):
                proName = cmds[lech + 4]
                proName = proName[1:len(proName)-3]
        if (tg == "" or scName == "" or pid == "" or proName == ""):
            tg = ""
        return tg, scName, pid, proName

    def processNumber(self):
        t = len(self.dt["process"].unique())
        print ("So process : " + str(t))
        return t
    def processGroupCount(self, min = 2):
        t = self.dt["process"].groupby([self.dt["process"]]).count().sort_values(0)
        t = pd.DataFrame(t)
        print (t.columns)
        #t.columns = ["process", "count"]
        print ("Thong ke theo process")
        print (t)

    def test(self):
        dt = self.dt
        dt = dt.groupby(["pid", "process"], as_index=True).count()
        print (dt)
    def layLoiPiD(self): #lay cac process khong co thong tin pid
        t  = self.dt.query("pid == ''")
        print ("danh sach cac tien trinh khong thu Pid")
        print (t)

    def laySyscallList(self):   # thong ke tan suat cac syscall trong log
        t = self.dt["syscall"].groupby(self.dt["syscall"]).count().sort_values(0)
        print("Thong ke cac syscall trong log")
        print(t)
        return t
    def tachLog(self):
        pl = self.dt["process"].unique()
        for pc in pl:
            l = self.dt[self.dt["process"] == pc]["syscall"]
            if (len(l) > 50):
                a = ScLog(pc, l)
                a.hienthi()


f = FirmadyneLog("v1.log")
f.processNumber()
f.tachLog()
#f.layLoiPiD()
#f.laySyscallList()
#f.test()
#f.processGroupCount()

def test():
    dt = ""
    pidgroup = dt.groupby(["pid","process"], as_index=True)
    print(dt.describe(include='all'))
    print(pidgroup["thoigian","syscall"].count().sort_values("syscall").query("syscall > 50").count())

    print (dt.shape)
    print (dt.query("process != process & pid != pid" ))
    #print(dt)
    #print ("Danh sach cac pid")
    #print (len(dt["process"].unique()))
#print(tachXau())


